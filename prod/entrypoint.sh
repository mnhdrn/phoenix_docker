#!/bin/sh
# Docker entrypoint script.

# Wait until Postgres is ready
while ! pg_isready -q -h $PGHOST -p $PGPORT -U $PGUSER
do
	echo "$(date) - waiting for database to start"
	sleep 2
done

MIX_ENV=prod mix ecto.drop && echo "DATABASE DROP"
MIX_ENV=prod mix ecto.create && echo "DATABASE CREATE"
MIX_ENV=prod mix ecto.migrate && echo "DATABASE MIGRATE"
MIX_ENV=prod mix run priv/repo/seeds.exs && echo "DATABASE SEED"
MIX_ENV=prod mix phx.server
